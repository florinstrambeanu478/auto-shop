- Ca sa functioneze aveti nevoie de PHP 7.3 si de composer pentru a instala dependintele

- Pentru a introduce un angajat trebuie scris in terminal php .\index.php apoi urmat de comanda add-employee
urmat de spatiu si urmatorele comenzi vor fi tipul de angajat(director, mechanic,assistant) plus prenume,nume, 
data nasterii, data angajarii dupa exemplul: 
php .\index.php add-employee mechanic George Armean 21-01-1989 01-02-2019

- Pentru a introduce o masina comanda initiala va fi add-car urmat de type(standard,bus,truck) cu datele
mileage(int) manufacturingYear, diesel(bool) si un unique trait pe care fiecare vehicul il are:
 standard->transmission(automat sau manual)
 bus->seatNumbers(int)
 truck->weight(float)

exemplu:
php .\index.php add-car truck 230000 23-10-1978 true 10

datele sunt salvate in fisiere de tip csv