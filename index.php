<?php

require 'vendor/autoload.php';
use App\Writers\CsvWriter;
use App\Readers\CsvReader;
//use App\Models\Director;
//use App\Models\Mechanic;
//use App\Models\StandardCar;
//use App\Models\Truck;
//use App\Writers\CsvWriter;
//use App\Models\Assistant;
//use App\Models\Bus;
//
//$bus = new Bus(278000, new DateTime('21-12-1995'), true, 67);
//$standard = new StandardCar(2333, new DateTime('21-12-1995'), true, 'manual');
//$truck = new Truck(1200, new DateTime('21-12-1995'), true, 8);
//
//$csvWriter = new CsvWriter();
//
////$csvWriter->write($bus);
//
//echo $standard->calculateInsurancePolicy(true);

//echo 'Welcome Management App V1.0';
//echo PHP_EOL;
//
//while (true) {
//    $command = cleanStr(fgets(STDIN));
//
//    switch ($command) {
//        case 'exit':
//            echo 'Have a nice day!';
//            break 2;
//        case 'add-director':
//
//        default:
//            echo "Command {$command} not available" . PHP_EOL;
//            break;
//    }
//}
//
//function cleanStr($string) {
//    $string = str_replace(' ', '-', $string);
//    $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
//    $string = preg_replace('/-+/', '-', $string);
//    return $string;
//}
//if (isset($argc)) {
//    for ($i = 0; $i < $argc; $i++) {
//        echo "Argument #" . $i . " - " . $argv[$i] . "\n";
//    }
//}
//else {
//    echo "argc and argv disabled\n";
//}

$command = $argv[1];

switch ($command) {
    case 'add-employee':
        addEmployee($argv[2], $argv[3], $argv[4], $argv[5], $argv[6]);
        echo 'Employee ' . $argv[3] . ' ' . $argv[4] . ' added successfully';
        break;
    case 'add-car':
        addCar($argv[2], $argv[3], $argv[4], $argv[5], $argv[6]);
        echo 'The ' .$argv[2] . ' added successfully';
        break;
    }

    function addCar($type, $mileage, $manufacturingYear, $diesel, $uniqueTrait) {
        $writer = new \App\Writers\CsvWriter();

        switch ($type) {
            case 'standard':
                $standard = new \App\Models\StandardCar( $mileage, new DateTime($manufacturingYear), $diesel, $uniqueTrait);
                $writer->write($standard);
                break;
            case 'bus':
                $standard = new \App\Models\Bus($mileage, new DateTime($manufacturingYear), $diesel, $uniqueTrait);
                $writer->write($standard);
                break;
            case 'truck':
                $standard = new \App\Models\Truck($mileage, new DateTime($manufacturingYear), $diesel, $uniqueTrait);
                $writer->write($standard);
                break;
        }
    }

function addEmployee($type, $firstName, $lastName, $dateOfBirth, $hiringDate) {
    $writer = new \App\Writers\CsvWriter();

    switch ($type) {
        case 'mechanic':
            $mechanic = new \App\Models\Mechanic($firstName, $lastName, new DateTime($dateOfBirth), new DateTime($hiringDate));
            $writer->write($mechanic);
            break;
        case 'director':
            $director = new \App\Models\Director($firstName, $lastName, new DateTime($dateOfBirth), new DateTime($hiringDate));
            $writer->write($director);
            break;
        case 'assistant':
            $assistant = new \App\Models\Assistant($firstName, $lastName, new DateTime($dateOfBirth), new DateTime($hiringDate));
            $writer->write($assistant);
            break;
    }
}



