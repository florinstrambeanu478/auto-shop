<?php

namespace App\Contracts;

interface Model
{
    public function getName(): string;

    public function toArray(): array;
}