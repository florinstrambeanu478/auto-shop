<?php


namespace App\Contracts;


interface Rules
{
    public function validate(Model $model): bool;

}