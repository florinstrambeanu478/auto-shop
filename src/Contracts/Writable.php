<?php

namespace App\Contracts;

interface Writable
{
    public function write(Model $model);
}