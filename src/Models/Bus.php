<?php


namespace App\Models;


use DateTime;

class Bus extends Car
{
    const NAME = 'buses';

    protected $seatNumbers = 0;

    public function __construct(
        int $mileage,
        DateTime $manufacturingYear,
        bool $diesel,
        int $seatNumbers
    ) {
        parent::__construct($mileage, $manufacturingYear, $diesel);

        $this->seatNumbers = $seatNumbers;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->mileage,
            $this->manufacturingYear->getTimestamp(),
            $this->diesel,
            $this->seatNumbers
        ];
    }

    public function calculateInsurancePolicy()
    {

    }
}