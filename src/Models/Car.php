<?php


namespace App\Models;

use DateTime;
use App\Contracts\Model;

abstract class Car implements Model
{
    protected $id;
    protected $mileage;
    protected $manufacturingYear;
    protected $diesel = false;

    public function __construct(int $mileage, DateTime $manufacturingYear, bool $diesel)
    {
        $this->id = time();
        $this->mileage = $mileage;
        $this->manufacturingYear = $manufacturingYear;
        $this->diesel = $diesel;
    }

    public abstract function calculateInsurancePolicy();
}