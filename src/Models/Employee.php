<?php

namespace App\Models;

use App\Contracts\Model;
use DateTime;

abstract class Employee implements Model
{
    protected $id;
    public $firstName;
    public $lastName;
    protected $dateOfBirth;
    protected $hiringDate;

    public function __construct(
        string $firstName,
        string $lastName,
        DateTime $dateOfBirth,
        DateTime $hiringDate
    ){

        if($firstName = '' || $lastName = ''){
            throw  new \Exception('name must not be null!');
        }

        $this->id = time();
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->dateOfBirth = $dateOfBirth;
        $this->hiringDate = $hiringDate;
    }

    public abstract function salary();

    public abstract function getName(): string;

    public function toArray(): array
    {
        return [
            $this->id,
            $this->firstName,
            $this->lastName,
            $this->salary(),
            $this->dateOfBirth->getTimestamp(),
            $this->hiringDate->getTimestamp(),
        ];
    }
}



