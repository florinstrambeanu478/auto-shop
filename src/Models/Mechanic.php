<?php

namespace App\Models;

use DateTime;

class Mechanic extends Employee
{
    const COEFFICIENT = 1.5;
    const NAME = 'mechanics';

    public function salary()
    {
        $diff = $this->hiringDate->diff(new DateTime('now'));

        $seniority = $diff->y > 1 ? $diff->y : 1;

        return $seniority * self::COEFFICIENT * 1000;
    }

    public function getName(): string
    {
        return self::NAME;
    }
}