<?php


namespace App\Models;


use DateTime;

class StandardCar extends Car
{
    const NAME = 'standard_cars';

    const TRANSMISSION_MANUAL = 'manual';
    const TRANSMISSION_AUTOMATIC = 'automatic';

    const TRANSMISSIONS = [
        self::TRANSMISSION_MANUAL,
        self::TRANSMISSION_AUTOMATIC
    ];

    protected $transmission = self::TRANSMISSION_MANUAL;

    public function __construct(int $mileage, DateTime $manufacturingYear, bool $diesel, string $transmission)
    {
        parent::__construct($mileage, $manufacturingYear, $diesel);

        if ($this->isValidTransmission($transmission)) {
            $this->transmission = $transmission;
        }
    }

    private function isValidTransmission(string $transmission) {
        return in_array($transmission, self::TRANSMISSIONS);
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->mileage,
            $this->manufacturingYear->getTimestamp(),
            $this->diesel,
            $this->transmission
        ];
    }

    public function calculateInsurancePolicy($discount = false)
    {
        $diff = $this->manufacturingYear->diff(new DateTime('now'));
        $ageOfCar = $diff->y > 1 ? $diff->y : 1;

        if ($this->mileage > 200000 && $this->diesel==true) {
            $policy = $ageOfCar * 100 + 500 + 500;
            return $discount ? $this->applyDiscount($policy) : $policy;
        } elseif ($this->mileage > 200000 || $this->diesel==true) {
            $policy = $ageOfCar * 100 + 500;
            return $discount ? $this->applyDiscount($policy) : $policy;
        }

        $policy = $ageOfCar * 100;
        return $discount ? $this->applyDiscount($policy) : $policy;
    }

    private function applyDiscount($policy)
    {
        return $policy - ($policy * 0.05);
    }
}