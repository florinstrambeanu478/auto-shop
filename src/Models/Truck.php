<?php

namespace App\Models;

use DateTime;

class Truck extends Car
{
    const NAME = 'trucks';

    protected $weight;

    public function __construct(
        int $mileage,
        DateTime $manufacturingYear,
        bool $diesel,
        float $weight
    ) {
        parent::__construct($mileage, $manufacturingYear, $diesel);

        $this->weight  = $weight;
    }

    public function getName(): string
    {
        return self::NAME;
    }

    public function toArray(): array
    {
        return [
            $this->id,
            $this->mileage,
            $this->manufacturingYear->getTimestamp(),
            $this->diesel,
            $this->weight
        ];
    }

    public function calculateInsurancePolicy($discount = false)
    {
        $diff = $this->manufacturingYear->diff(new DateTime('now'));

        $ageOfCar = $diff->y > 1 ? $diff->y : 1;

        if ($this->mileage > 800000) {
            $policy = $ageOfCar * 300 + 700;
            return $discount ? $this->applyDiscount($policy) : $policy;
        }

        $policy = $ageOfCar * 300;
        return $discount ? $this->applyDiscount($policy) : $policy;
    }

    private function applyDiscount($policy)
    {
        return $policy - ($policy * 0.15);
    }
}