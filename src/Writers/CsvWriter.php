<?php


namespace App\Writers;


use App\Contracts\Model;
use App\Contracts\Writable;

class CsvWriter implements Writable
{
    public function write(Model $model)
    {
        $handle = fopen($model->getName() . '.csv', 'a');

        fputcsv($handle, PHP_EOL);
        fputcsv($handle, $model->toArray());

        fclose($handle);
    }
}